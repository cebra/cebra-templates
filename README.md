# CEBRA Templates

This repository has been set up to create suitable $\LaTeX$ templates for CEBRA reports.

The main style file is [`cebra.sty`](cebra.sty).

You need both `cebra.sty` and `CEBRALogo-01.pdf` in the folder of the main tex/Rnw document. An alternative is to store these files in your local `texmf` tree (see [local installation below](#installation-via-symlink)).

## Style notes

- main font: palatino
    - traditional computer modern fonts can be used with the `plain` option
    - \usepackage[plain]{cebra}
- includes the *recommendation* environment by Matt Chisholm
- add a watermark by using option `drafting` in the package options
    - \usepackage[drafting]{cebra}
- includes a custom titlepage by Steve Lane
- includes a code examples listing for Rnw documents
- there are two versions: one with `-chapters` and one without
    - the version with `-chapters` uses `scrreport` as the document class, which gives a formal report style, with chapters as the top-level sectioning which when compiled, start on new pages
    - the version without `-chapters` uses `scrartcl`, which gives a more compact style, similar to a journal article; top-level sectioning uses sections, and there are new page breaks for new sections

## Local installation

### Installation via symlink

Even better than copying the files (see below), is to create a symlink to the files that are needed. This way, anytime you update the repository, latex will use the most recent versions. Let's assume you cloned the template directory into `~/cebra-templates`. Then to create a symlink to the required files for latex do the following:

```{bash}
> mkdir -p ~/Library/texmf/tex/latex/cebra
> ln -s ~/cebra-templates/CEBRALogo-01.pdf ~/Library/texmf/tex/latex/cebra/CEBRALogo-01.pdf
> ln -s ~/cebra-templates/cebra.sty ~/Library/texmf/tex/latex/cebra/cebra.sty
```

### Installation via copy

You can install this style locally by copying the `cebra.sty` and `CEBRALogo-01.pdf` files into your local texmf tree. This means that you don't need these files in every directory you're compiling a document.

On a mac:

```{bash}
> mkdir -p ~/Library/texmf/tex/latex/cebra
> cp cebra.sty CEBRALogo-01.pdf ~/Library/texmf/tex/latex/cebra
```

For the perennially lazy, running `./install_cebra` from the cloned directory will achieve the same thing.
