<<setup,echo=FALSE,warning=FALSE,message=FALSE,cache=FALSE,results="hide">>=
knitr::opts_chunk$set(cache = FALSE, error = FALSE, warning = FALSE,
                      message = FALSE, fig.align = "center", echo = FALSE,
                      out.width = "0.95\\textwidth")
knitr::opts_knit$set(out.format = "latex")
knitr::knit_theme$set("acid")
knitr::knit_hooks$set(rexample = function(before, options, envir) {
    cap <- ifelse(is.null(options$fig.cap), '', options$fig.cap)
    if (before){
        sprintf('\\begin{rexample}[%s]\\label{%s}\\hfill{}', cap,
                options$label)
    } else {
        '\\end{rexample}'
    }
})
options(digits = 1)

@ 

\documentclass{beamer}
% Use the metropolis theme
\usetheme[progressbar=frametitle]{metropolis}

% booktabs for better tables
\usepackage{booktabs}
% add the logo (must be in the same directory as this file, or somewhere in your path where tex can find it)
\usetikzlibrary{positioning}
\titlegraphic{
  \begin{tikzpicture}[overlay, remember picture]
    \node[at=(current page.south east), anchor=south east] {%
      \includegraphics[width=.3\textwidth]{CEBRALogo-01}
    };
  \end{tikzpicture}
}

% Title goes here
\title{My awesome title}
\subtitle{My subtitle}
\date{\today}
\author{Me}
\institute{Centre of Excellence for Biosecurity Risk Analysis\\The University of Melbourne}

\begin{document}

\maketitle

% Sections look nicer with an alternative section slide. This one makes it dark.

{
\metroset{background=dark}
\section{First Section}
\metroset{background=light}
}

\begin{frame}
  \frametitle{This frame's title}
  \begin{itemize}
  \item My first item
  \item My second item
  \end{itemize}
\end{frame}

\end{document}
